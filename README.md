# Clean Code i Java Program
### 2021 September


# Links
* [Installation Instructions](./installation-instructions.md)
* [Zoom Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md) 
* [Course Details](https://www.ribomation.se/courses/jvm/clean-code-in-java)


# Usage of this GIT Repo
Ensure you have a [GIT client](https://git-scm.com/downloads) installed and clone this repo. 

    mkdir -p ~/clean-code-course/my-solutions
    cd ~/clean-code-course
    git clone <https url to this repo> gitlab

During the course, solutions will be push:ed to this repo and you can get these by a `git pull` operation

    cd ~/clean-code-course/gitlab
    git pull

# Build Solution/Demo Programs
All programs are ordinary Java programs and can be compiled using any appropriate tool.
Several of the demo programs has a Gradle build file and can therefore be built by

    gradle build


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

