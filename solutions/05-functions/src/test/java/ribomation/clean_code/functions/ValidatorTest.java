package ribomation.clean_code.functions;

import org.junit.Before;
import org.junit.Test;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ValidatorTest {
    private Validator target;

    @Before
    public void init() {
//        GregorianCalendar date = new GregorianCalendar(2015, Calendar.MARCH, 15);
//        target = new Validator(date);
        target = new Validator();
    }

    @Test
    public void with_13_chars_should_remove_minus() {
        assertThat(target.normalize("19750415-4455"), is("197504154455"));
    }

    @Test
    public void with_13_chars_should_remove_plus() {
        assertThat(target.normalize("19750415+4455"), is("197504154455"));
    }

    @Test
    public void with_12_chars_should_return() {
        assertThat(target.normalize("197504154455"), is("197504154455"));
    }

    @Test
    public void with_11_chars_and_plus_should_prepend_19_and_remove_plus() {
        assertThat(target.normalize("750415+4455"), is("197504154455"));
    }

    @Test
    public void with_11_chars_and_minus_should_prepend_19_and_remove_minus() {
        assertThat(target.normalize("750415-4455"), is("197504154455"));
    }

    @Test
    public void with_10_chars_and_minus_should_prepend_19_and_remove_minus() {
        assertThat(target.normalize("7504154455"), is("197504154455"));
    }

    @Test
    public void with_10_chars_and_born_this_year_should_prepend_20_and_remove_minus() {
        assertThat(target.normalize("1505154455"), is("201505154455"));
    }

}
