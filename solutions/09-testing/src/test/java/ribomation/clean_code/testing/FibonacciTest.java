package ribomation.clean_code.testing;

import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class FibonacciTest {
    private Fibonacci target;

    @Before
    public void setUp() throws Exception {
        target = new Fibonacci();
    }

    @Test
    public void simple_args_should_pass() throws Exception {
        assertThat(target.compute(2), is(BigInteger.ONE));
        assertThat(target.compute(5), is(BigInteger.valueOf(5)));
        assertThat(target.compute(10), is(BigInteger.valueOf(55)));
    }

    @Test
    public void arg_42_should_pass() throws Exception {
        assertThat(target.compute(42), is(BigInteger.valueOf(267_914_296)));
    }

    @Test
    public void large_arg_should_pass() throws Exception {
        String result = "354224848179261915075";
        assertThat(target.compute(100), is(new BigInteger(result)));
    }
}
