package ribomation.clean_code.testing;

import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class CachingUnaryFunctionTest {
    public static final BigInteger VALUE_42 = BigInteger.valueOf(42);
    private CachingUnaryFunction target;

    @Before
    public void setUp() throws Exception {
        target = new CachingUnaryFunction() {
            @Override
            protected BigInteger computeResult(int arg) {
                return VALUE_42;
            }
        };
    }

    @Test(expected = IllegalArgumentException.class)
    public void negative_value_should_throw() throws Exception {
        target.compute(-5);
    }

    @Test
    public void zero_and_one_should_return_identy() {
        assertThat(target.compute(0), is(BigInteger.ZERO));
        assertThat(target.compute(1), is(BigInteger.ONE));
    }

    @Test
    public void any_other_should_return_42() {
        assertThat(target.compute(2), is(VALUE_42));
        assertThat(target.compute(20), is(VALUE_42));
        assertThat(target.compute(200), is(VALUE_42));
    }

}