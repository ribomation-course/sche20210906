package com.ribomation.clean_code;

import java.util.stream.Stream;

/**
 * Fake transport service
 *
 * @author Jens Riboe, jens.riboe@ribomation.se
 * @date 2016-08-18
 */
public class DummyEmailTransport implements EmailTransport {

    @Override
    public void send(Message message) {
        System.out.println("*** Pretending to send email ***");
        Stream.of("from", "to", "cc", "subject", "body")
                .map(key -> String.format("%-7s: %s", key, get(key, message)))
                .forEach(System.out::println);
        ;
    }

    String get(String key, Message m) {
        String methodName = "get" + key.substring(0, 1).toUpperCase() + key.substring(1);
        try {
            return (String) m.getClass().getDeclaredMethod(methodName).invoke(m);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
