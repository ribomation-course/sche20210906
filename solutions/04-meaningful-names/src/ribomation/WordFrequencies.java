package ribomation;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class WordFrequencies {
    public static final String MUSKETEERS_TXT = "./data/the-three-musketeers.txt";
    public static final int MIN_WORD_SIZE = 5;
    public static final int MAX_WORDS = 25;
    public static final String DELIM_PATTERN = "[^a-zA-Z]+";

    public static void main(String[] args) throws Exception {
        WordFrequencies app = new WordFrequencies();
        app.runAlt(openStream(args));
    }

    static InputStream openStream(String[] args) throws IOException {
        if (args.length == 0) {
            return Files.newInputStream(Path.of(MUSKETEERS_TXT));
        } else {
            return Files.newInputStream(Path.of(args[0]));
        }
    }

    void runAlt(InputStream is) throws IOException {
        printWordFreqsAlt(loadWordFreqsAlt(is, MIN_WORD_SIZE), MAX_WORDS);
    }

    Map<String, Long> loadWordFreqsAlt(InputStream is, int minWordSize) throws IOException {
        final Pattern delimPattern = Pattern.compile(DELIM_PATTERN);
        var input = new BufferedReader(new InputStreamReader(is));
        try (input) {
            return input.lines()
                    .flatMap(delimPattern::splitAsStream)
                    .filter(word -> word.length() >= minWordSize)
                    .map(String::toLowerCase)
                    .collect(Collectors.groupingBy(word -> word, Collectors.counting()))
                    ;
        }
    }

    void printWordFreqsAlt(Map<String, Long> wordFreqs, int maxWords) {
        wordFreqs.entrySet().stream()
                .sorted((lhs, rhs) -> Long.compare(rhs.getValue(), lhs.getValue()))
                .limit(maxWords)
                .forEach(e -> System.out.printf("%12s: %d%n", e.getKey(), e.getValue()))
        ;
    }


    void run(InputStream is) throws IOException {
        var wordFreqs = loadWordFreqs(is, MIN_WORD_SIZE);
        printWordFreqs(wordFreqs, MAX_WORDS);
    }

    Map<String, Integer> loadWordFreqs(InputStream is, int minWordSize) throws IOException {
        var freqs = new HashMap<String, Integer>();
        var input = new BufferedReader(new InputStreamReader(is));
        for (var line = input.readLine(); line != null; line = input.readLine()) {
            for (var word : line.split(DELIM_PATTERN)) {
                if (word.length() < minWordSize) continue;
                word = word.toLowerCase();
                freqs.put(word, freqs.getOrDefault(word, 0) + 1);
            }
        }
        return freqs;
    }

    void printWordFreqs(Map<String, Integer> wordFreqs, int maxWords) {
        List<Map.Entry<String, Integer>> wordFreqPairs = new ArrayList<>(wordFreqs.entrySet());
        wordFreqPairs.sort((lhs, rhs) -> Long.compare(rhs.getValue(), lhs.getValue()));

        int numWords = 0;
        for (var e : wordFreqPairs) {
            System.out.printf("%12s: %d%n", e.getKey(), e.getValue());
            if (++numWords >= maxWords) return;
        }
    }

}
