package ribomation.clean_code.data_structures;

public class Square extends Rectangle {
    public Square(Point position, int side) {
        super(position, side, side);
    }

    @Override
    protected String dimension() {
        return String.format("%d", width);
    }
}
