package ribomation.clean_code.data_structures;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class ShapeFactory {
    public static List<Shape> loadShapes(String filename) throws IOException {
        return Files.lines(Paths.get(filename))
                .skip(1)
                .map(ShapeFactory::fromCSV)
                .collect(Collectors.toList());
    }

    public static Shape fromCSV(String csv) {
        String[] fields = csv.split(",");
        String   type   = fields[0];
        int      x      = Integer.parseInt(fields[1]);
        int      y      = Integer.parseInt(fields[2]);
        int      w      = Integer.parseInt(fields[3]);
        int      h      = Integer.parseInt(fields[4]);

        Point loc = new Point(x, y);
        switch (type) {
            case "rect":
                return new Rectangle(loc, w, h);
            case "squa":
                return new Square(loc, w);
            case "tria":
                return new Triangle(loc, w, h);
            case "circ":
                return new Circle(loc, w);
        }

        throw new IllegalArgumentException("no such type: " + type);
    }

}
