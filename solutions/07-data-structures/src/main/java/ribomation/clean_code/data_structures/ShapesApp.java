package ribomation.clean_code.data_structures;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class ShapesApp {
    public static void main(String[] args) throws Exception {
        ShapesApp app = new ShapesApp();
        app.run(args);
    }

    void run(String[] args) throws IOException {
        String    filename       = "./src/main/resources/shapes.csv";
        final int maxShapesCount = 15;
        if (args.length > 0) {
            filename = args[0];
        }

        List<Shape> shapes = ShapeFactory.loadShapes(filename);

        shapes.stream()
                .limit(maxShapesCount)
                .map(Shape::toString)
                .forEach(System.out::println);

        System.out.printf(Locale.ENGLISH, "Total area = %.3f%n",
                shapes.stream()
                        .mapToDouble(Shape::area)
                        .sum()
                );

        System.out.printf(Locale.ENGLISH, "Total perimeter = %.3f%n",
                shapes.stream()
                        .mapToDouble(Shape::perimeter)
                        .sum()
        );
    }
}
