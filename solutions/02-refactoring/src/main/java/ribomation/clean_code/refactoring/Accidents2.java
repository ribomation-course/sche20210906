package ribomation.clean_code.refactoring;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Stream;

public class Accidents2 {
    public static void main(String[] args) throws Exception {
        Accidents2 app = new Accidents2();

        final String url = "https://raw.githubusercontent.com/devharis/Global-Crime/master/source/swedenXML.xml";
        //NodeList list = app.fetchByUrl(url);

        final String path = "/swedenXML.xml";
        NodeList list = app.fetchByClasspathResource(path);

        app.print(list);
    }

    NodeList fetchByUrl(String url, int _lastHours) throws ParserConfigurationException, IOException, SAXException {
        return fetch(new URL(url).openStream());
    }

    NodeList fetchByClasspathResource(String path) throws ParserConfigurationException, IOException, SAXException {
        return fetch(this.getClass().getResourceAsStream(path));
    }

    NodeList fetch(InputStream is) throws ParserConfigurationException, IOException, SAXException {
        Document doc = DocumentBuilderFactory
                .newInstance()
                .newDocumentBuilder()
                .parse(is);
        doc.getDocumentElement().normalize();
        return doc.getElementsByTagName("event");
    }

    void print(NodeList events) {
        int[] index = {0};
        Stream.generate(() -> events.item(index[0]++))
                .limit(events.getLength())
                .map(Event::new)
                .forEach(System.out::println);
        ;
    }

    static class Event {
        final SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String title, place, text, link;
        Date   date;
        double latitude, longitude;

        public Event(Node eventXml) {
            this((Element) eventXml);
        }

        public Event(Element eventXml) {
            title = textOf(eventXml, "title");
            place = textOf(eventXml, "place");
            link = textOf(eventXml, "link").replaceAll("[\r\n\t ]+", "");
            text = textOf(eventXml, "text").replaceAll("\\s+", " ");
            latitude = Double.parseDouble(textOf(eventXml, "lat"));
            longitude = Double.parseDouble(textOf(eventXml, "lng"));
            try {
                date = fmt.parse(textOf(eventXml, "date"));
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
        }

        private String textOf(Element e, String tag) {
            NodeList list = e.getElementsByTagName(tag);
            if (list.getLength() == 0) return "";
            return list.item(0).getTextContent();
        }

        @Override
        public String toString() {
            return String.format("*** %s: %s%n\t%s (%.4f,%.4f)%n\t%s%n\t<%s>%n",
                    date, title, place, latitude, longitude, text, link);
        }
    }
}
