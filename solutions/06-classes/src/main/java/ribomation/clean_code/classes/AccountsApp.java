package ribomation.clean_code.classes;

import ribomation.clean_code.classes.domain.AccountRepo;
import ribomation.clean_code.classes.domain.AccountTableModel;

public class AccountsApp {
    public static void main(String[] args) throws Exception {
        AccountsApp app = new AccountsApp();
        app.run(args);
    }

    void run(String[] args) {
        MetaData metaData = new MetaData();
        var repo = new AccountRepo();
        var model = new AccountTableModel(repo);
        MainWindow gui = new MainWindow(model, metaData, repo.getFilename());
        gui.showGui();
    }

}
