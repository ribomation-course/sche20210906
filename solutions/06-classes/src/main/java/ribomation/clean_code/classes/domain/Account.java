package ribomation.clean_code.classes.domain;

public class Account {
//    iban,amount,currency,owner,profession,country
//    FR77 9812 4565 794C TEIT AYE0 740,99.03,BRL,Sharon Meyer,Electrical Engineer,Brazil

    private final String iban;
    private final double amount;
    private final String currency;
    private final String owner;
    private final String profession;
    private final String country;

    public Account(String iban, double amount, String currency, String owner, String profession, String country) {
        this.iban = iban;
        this.amount = amount;
        this.currency = currency;
        this.owner = owner;
        this.profession = profession;
        this.country = country;
    }

    public int getCount() {
        return 6;
    }

    public Object getValue(int index) {
        switch (index) {
            case 0: return iban;
            case 1: return amount;
            case 2: return currency;
            case 3: return owner;
            case 4: return profession;
            case 5: return country;
        }
        throw new IndexOutOfBoundsException(index);
    }

    public String getName(int index) {
        switch (index) {
            case 0: return "iban";
            case 1: return "amount";
            case 2: return "currency";
            case 3: return "owner";
            case 4: return "profession";
            case 5: return "country";
        }
        throw new IndexOutOfBoundsException(index);
    }

    public Class<?> getClass(int index) {
        return String.class;
    }

    public String getIban() {
        return iban;
    }

    public double getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getOwner() {
        return owner;
    }

    public String getProfession() {
        return profession;
    }

    public String getCountry() {
        return country;
    }
}
