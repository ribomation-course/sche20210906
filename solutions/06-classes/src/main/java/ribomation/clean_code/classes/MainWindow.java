package ribomation.clean_code.classes;

import ribomation.clean_code.classes.domain.AccountTableModel;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import java.awt.*;

public class MainWindow extends JFrame {
    public MainWindow(AccountTableModel model, MetaData metaData, String filename) throws HeadlessException {
        this.setLayout(new BorderLayout());
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        updateWindowTitle(metaData);
        setupLayout(model, filename);
    }

    public void showGui() {
        this.pack();
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }

    private void updateWindowTitle(MetaData metaData) {
        this.setTitle(String.format("%s :: Version %d (%s) :: (C) %s, %d.",
                metaData.getAppName(),
                metaData.getVersion(), metaData.getDate(), metaData.getCompany(), metaData.getYear()));
    }

    private void setupLayout(AccountTableModel model, String filename) {
        JPanel                     info   = new InfoPane(filename, model, 4);
        JTable                     tbl    = createTable(model);
        TableRowSorter<TableModel> sorter = new TableRowSorter<>(model);
        tbl.setRowSorter(sorter);
        JPanel filterPane = createFilterPane(sorter);

        this.getContentPane().add(info, BorderLayout.NORTH);
        this.getContentPane().add(new JScrollPane(tbl), BorderLayout.CENTER);
        this.getContentPane().add(filterPane, BorderLayout.SOUTH);
    }

    private JTable createTable(TableModel model) {
        JTable tbl = new JTable(model);
        tbl.setAutoCreateRowSorter(true);
        tbl.setFillsViewportHeight(true);
        tbl.setPreferredScrollableViewportSize(new Dimension(800, 500));

        final int N = model.getColumnCount();
        for (int k = 0; k < N; ++k) {
            Component   c   = tbl.getDefaultRenderer(model.getColumnClass(k)).getTableCellRendererComponent(tbl, model.getValueAt(0, k) /*data.get(1)[k]*/, false, false, 1, k);
            TableColumn col = tbl.getColumnModel().getColumn(k);
            col.setMinWidth(c.getPreferredSize().width);

            if (model.getColumnName(k).equalsIgnoreCase("amount")) {
                DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
                renderer.setHorizontalAlignment(JLabel.RIGHT);
                col.setCellRenderer(renderer);
            }

            if (model.getColumnName(k).equalsIgnoreCase("currency")) {
                DefaultTableCellRenderer renderer = new DefaultTableCellRenderer();
                renderer.setHorizontalAlignment(JLabel.CENTER);
                col.setCellRenderer(renderer);
            }
        }

        return tbl;
    }

    private JPanel createFilterPane(TableRowSorter<TableModel> sorter) {
        final JTextField filter = new JTextField();
        filter.getDocument().addDocumentListener(new DocumentListener() {
            void init() {
                String                        phrase = filter.getText();
                RowFilter<TableModel, Object> rf     = javax.swing.RowFilter.regexFilter(phrase, 0, 1, 2, 3, 4);
                sorter.setRowFilter(rf);
            }

            public void changedUpdate(DocumentEvent e) {
                init();
            }

            public void insertUpdate(DocumentEvent e) {
                init();
            }

            public void removeUpdate(DocumentEvent e) {
                init();
            }
        });

        JPanel form = new JPanel(new SpringLayout());
        JLabel lbl  = new JLabel("Filter column values:", SwingConstants.TRAILING);
        lbl.setLabelFor(filter);
        form.add(lbl);
        form.add(filter);
        SpringUtilities.makeCompactGrid(form, 1, 2, 6, 6, 6, 6);

        return form;
    }

}
