package ribomation.clean_code.classes;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Encapsulates app and build info
 */
public class MetaData {
    String getAppName() {
        return "AccountsSwingApp";
    }

    int getVersion() {
        return 42;
    }

    String getCompany() {
        return "Foobar Ltd";
    }

    String getDate() {
        return new SimpleDateFormat("d MMM yyyy").format(new Date());
    }

    int getYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }
}
