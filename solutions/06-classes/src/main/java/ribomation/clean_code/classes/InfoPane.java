package ribomation.clean_code.classes;

import ribomation.clean_code.classes.domain.AccountTableModel;

import javax.swing.*;
import java.util.Locale;

/**
 * Upper information Swing pane
 */
public class InfoPane extends JPanel {

    public InfoPane(String filename, AccountTableModel model, int numRows) {
        super(new SpringLayout());

        createPane("Account File", filename);
        createPane("SUM Amount", format(model.getAmountSum()));
        createPane("# Currencies", format(model.getCurrencyCount()));
        createPane("# Countries", format(model.getCountryCount()));

        SpringUtilities.makeCompactGrid(this, numRows, 2, 6, 6, 6, 6);
    }

    private String format(long value) {
        return String.format(Locale.ENGLISH, "%d", value);
    }

    private String format(double value) {
        return String.format(Locale.ENGLISH, "%.2f", value);
    }

    private void createPane(String label, String value) {
        JLabel l = new JLabel(label + ":", JLabel.TRAILING);
        this.add(l);
        JTextField f = new JTextField(16);
        f.setEditable(false);
        l.setLabelFor(f);
        this.add(f);
        f.setText(value);
    }

}
