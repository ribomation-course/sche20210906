package meaningful_names;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class First {
    public static void main(String[] args) {
        First app = new First();
        app.init();
        app.run();
    }

    int[][] lst;

    List<int[]> getThem() {
        List<int[]> lst1 = new ArrayList<>();
        for (int[] x : lst)
            if (x[0] == 4)
                lst1.add(x);
        return lst1;
    }

    void run() {
        List<int[]> y = getThem();
        System.out.println("y = \n" + s(y));
    }

    String s(List<int[]> Z) {
        String t = "";
        for (int[] z : Z)
            t += Arrays.toString(z) + "\n";
        return t;
    }

    void init() {
        Random r = new Random();
        lst = new int[32][];
        for (int k = 0; k < lst.length; k++) {
            lst[k] = new int[4];
            lst[k][0] = r.nextInt(6);
        }
    }
}
