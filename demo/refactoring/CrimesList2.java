package refactoring;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CrimesList2 {
    private static final String URL              = "http://brottsplatskartan.se/api.php?action=getEvents&period=";
    public static final  int    MINUTES_PER_HOUR = 60;

    public static void main(String[] args) throws Exception {
        CrimesList2 app = new CrimesList2();
        app.run(args);
    }

    private void run(String[] args) throws Exception {
        int    lastHours = args.length > 0 ? Integer.parseInt(args[0]) : 72;
        String crimeType = args.length > 1 ? args[1] : "trafikolycka";

        String   url = URL + (lastHours * MINUTES_PER_HOUR);
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(url);
        doc.getDocumentElement().normalize();

        NodeList       events = doc.getElementsByTagName("event");
        List<String[]> crimes = new ArrayList<>(events.getLength());
        for (int i = 0; i < events.getLength(); i++) {
            Element e     = (Element) events.item(i);
            String  title = textOf(e, "title");
            String  text  = textOf(e, "text");
            String  date  = textOf(e, "date");
            String  place = textOf(e, "place");

            if (isInteresting(crimeType, title, text)) {
                String[] data = {title, text, date, place};
                crimes.add(data);
            }
        }

        System.out.printf("Det har inträffat %d händelser av typ '%s' de senaste %d timmarna.%n",
                crimes.size(), crimeType, lastHours);
        for (String[] crime : crimes) {
            SimpleDateFormat iso8601 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date             date    = iso8601.parse(crime[2]);
            System.out.printf("%1$te %1$tb %1$tY, %1$tT - %2$s (%3$s)%n",
                    date, crime[0], crime[3]);
        }
    }

    private String textOf(Element e, String tag) {
        NodeList nodes = e.getElementsByTagName(tag);
        if (nodes.getLength() > 0) {
            return nodes.item(0).getTextContent();
        }
        return "";
    }

    private boolean isInteresting(String crimeType, String title, String text) {
        return title.toLowerCase().contains(crimeType) || text.toLowerCase().contains(crimeType);
    }
}
