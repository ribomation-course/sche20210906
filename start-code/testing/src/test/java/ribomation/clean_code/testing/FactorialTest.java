package ribomation.clean_code.testing;

import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class FactorialTest {
    private Factorial target;

    @Before
    public void setUp() throws Exception {
        target = new Factorial();
    }

    @Test
    public void simple_args_should_pass() throws Exception {
        assertThat(target.compute(4), is(BigInteger.valueOf(2 * 3 * 4)));
        assertThat(target.compute(5), is(BigInteger.valueOf(2 * 3 * 4 * 5)));
        assertThat(target.compute(6), is(BigInteger.valueOf(2 * 3 * 4 * 5 * 6)));
    }

    @Test
    public void large_arg_should_pass() throws Exception {
        String result = "93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000";
        assertThat(target.compute(100), is(new BigInteger(result)));
    }
}