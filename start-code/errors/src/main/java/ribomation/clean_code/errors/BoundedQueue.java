package ribomation.clean_code.errors;

import java.util.concurrent.atomic.AtomicBoolean;

public class BoundedQueue<T> {
    private CircularBuffer<T> queue;
    private AtomicBoolean done = new AtomicBoolean(false);

    public BoundedQueue(int capacity) {
        queue = new CircularBuffer<>(capacity);
    }

    public synchronized void put(T x) {
        while (queue.full()) {
            try {
                wait();
            } catch (InterruptedException ignore) {
            }
        }

        int rc = queue.put(x);
        if (rc != 0) {
            System.err.println("failed to insert element into queue");
            System.exit(1);
        }
        notifyAll();
    }

    public synchronized T get() {
        while (queue.empty()) {
            try {
                wait();
            } catch (InterruptedException ignore) {
            }
        }

        @SuppressWarnings("unchecked")
        T[] result = (T[]) new Object[1];
        int rc = queue.get(result);
        if (rc != 0) {
            System.err.println("failed to extract element from queue");
            System.exit(1);
        }
        notifyAll();

        return result[0];
    }

    public void setDone() {
        done.set(true);
    }

    public boolean isDone() {
        return queue.empty() && done.get();
    }
}
