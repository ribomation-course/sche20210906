package ribomation.clean_code.names;

import java.io.*;
import java.util.*;

public class wf {
    public static void main(String[] x) throws Exception {
        new wf().r(i(x));
    }

    static InputStream i(String[] a) throws FileNotFoundException {
        if (a.length == 1) {
            return new FileInputStream(a[0]);
        } else {
            return wf.class.getResourceAsStream("/ttm.txt");
        }
    }

    void r(InputStream is) throws IOException {
        p(l(is, 4), 20);
    }

    Map<String, Integer> l(InputStream is, int mws) throws IOException {
        Map<String, Integer> f = new HashMap<>();
        BufferedReader       b = new BufferedReader(new InputStreamReader(is));
        for (String s = b.readLine(); s != null; s = b.readLine()) {
            for (String w : s.split("[^a-zA-Z]+")) {
                if (w.length() < mws) continue;
                w = w.toLowerCase();
                f.put(w, f.getOrDefault(w, 0) + 1);
            }
        }
        return f;
    }

    void p(Map<String, Integer> w, int N) {
        List<Map.Entry<String, Integer>> f = new ArrayList<>(w.entrySet());

        Collections.sort(f, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return Integer.compare(o2.getValue(), o1.getValue());
            }
        });

        int n = 0;
        for (Map.Entry<String, Integer> e : f) {
            System.out.printf("%12s: %d%n", e.getKey(), e.getValue());
            if (++n >= N) return;
        }
    }

}
