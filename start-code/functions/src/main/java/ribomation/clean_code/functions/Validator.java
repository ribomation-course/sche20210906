package ribomation.clean_code.functions;


import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Validator {
    private Calendar date;

    public Validator(Calendar date) {
        this.date = date;
    }

    String normalize(String pnr) {
        if (pnr.length() != 12) {
            if (pnr.length() != 13) {
                if (!pnr.contains("+")) {
                    pnr = pnr.replaceFirst("[-]", "");
                    int birthYear   = Integer.parseInt(pnr.substring(0, 2));
                    int currentYear = date.get(Calendar.YEAR);
                    if (currentYear >= (2000 + birthYear)) {
                        SimpleDateFormat fmt      = new SimpleDateFormat("yyyyMMdd");
                        String           todayStr = fmt.format(date.getTime());
                        String           birthStr = "20" + pnr.substring(0, 6);
                        if (currentYear != (2000 + birthYear) || birthStr.compareTo(todayStr) > 0) {
                            return "20" + pnr;
                        } else {
                            return "19" + pnr;
                        }
                    } else {
                        return "19" + pnr;
                    }
                } else {
                    return "19" + pnr.replaceFirst("[+]", "");
                }
            } else {
                return pnr.replaceFirst("[+-]", "");
            }
        } else {
            return pnr;
        }
    }

}

